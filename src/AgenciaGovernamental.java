import java.util.ArrayList;


public class AgenciaGovernamental extends Usuario{

	private ArrayList<Comunidade> listaComunidades;
	private ArrayList<Post> listaPosts;
	
	

	public AgenciaGovernamental(String umNome, String umEmail) {
		super(umNome, umEmail);
	}
	
	public void VisualizarPost(){
		System.out.println(getListaPosts());
	}
	
	public void VisualizarComunidade(){
		System.out.println(getListaComunidades());
	}
	
	public ArrayList<Comunidade> getListaComunidades() {
		return listaComunidades;
	}

	public void setListaComunidades(ArrayList<Comunidade> listaComunidades) {
		this.listaComunidades = listaComunidades;
	}

	public ArrayList<Post> getListaPosts() {
		return listaPosts;
	}

	public void setListaPosts(ArrayList<Post> listaPosts) {
		this.listaPosts = listaPosts;
	}
}
