import java.util.ArrayList;


public class Comunidade {
	
	private String nomeComunidade;
	private ArrayList<Comunidade> listaComunidades;
	private ArrayList<Usuario> listaUsuarios;
	
	public Comunidade(String umNome){
		this.nomeComunidade = umNome;
		this.listaUsuarios = new ArrayList<Usuario>();		
	}
	
	public void controleComunidade(){
		this.listaComunidades = new ArrayList<Comunidade>();
		this.listaUsuarios = new ArrayList<Usuario>();
	}
	
	public void AdicionarComunidade(Comunidade umaComunidade){
		this.listaComunidades.add(umaComunidade);
	}
	
	public void AdicionarUsuario(Usuario umUsuario, Comunidade umaComunidade){
		this.listaUsuarios.add(umUsuario);
		umaComunidade.setListaUsuarios(this.listaUsuarios);
		this.listaComunidades.remove(umaComunidade);
		this.listaComunidades.add(umaComunidade);
	}

	public ArrayList<Usuario> getListaUsuariosComunidade(Comunidade umaComunidade){
		return umaComunidade.getListaUsuarios();
	}
	
	public ArrayList<Comunidade> getListaComunidades() {
		return listaComunidades;
	}

	public void setListaComunidades(ArrayList<Comunidade> listaComunidades) {
		this.listaComunidades = listaComunidades;
	}

	public ArrayList<Usuario> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(ArrayList<Usuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public String getNomeComunidade() {
		return nomeComunidade;
	}

	public void setNomeComunidade(String nomeComunidade) {
		this.nomeComunidade = nomeComunidade;
	}
}
