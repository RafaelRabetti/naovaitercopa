import java.util.ArrayList;

public class Usuario {
	
	private String nome;
	private String senha;
	private String email;
	private String pais;
	private String estado;
	private String cidade;
	private String atividadeProfissional;
	private String organizacao;
	private ArrayList<Usuario> listaUsuarios;
	
	public Usuario(String umNome, String umEmail){
		this.nome = umNome;
		this.email = umEmail;
	}
	
	public void adicionar(Usuario umUsuario){
		this.listaUsuarios.add(umUsuario);
	}
			
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getAtividadeProfissional() {
		return atividadeProfissional;
	}
	public void setAtividadeProfissional(String atividadeProfissional) {
		this.atividadeProfissional = atividadeProfissional;
	}
	public String getOrganizacao() {
		return organizacao;
	}
	public void setOrganizacao(String organizacao) {
		this.organizacao = organizacao;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}


}
