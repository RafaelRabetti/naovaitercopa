import static org.junit.Assert.*;
import static org.junit.Assert.fail;

import org.junit.BeforeClass;
import org.junit.Test;



public class Testes {
	
	static Usuario novoControleUsuario;
	static Usuario novoUsuario;
	static Comunidade novoControleComunidade;
	static Comunidade novaComunidade;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		novoControleComunidade = new Comunidade("comunidade");
		novoControleUsuario = new Usuario("jao","l@a");
		novaComunidade= new Comunidade("comunas");
		novoUsuario = new Usuario("joao", "l@a");
		novaComunidade.setNomeComunidade("OO");
	}
	
	
	@Test
	public void adicionarTest() {
		novoControleComunidade.AdicionarComunidade(novaComunidade);
		novoControleUsuario.adicionar(novoUsuario);
		novoControleComunidade.AdicionarUsuario(novoUsuario, novaComunidade);
		assertNotNull(novoControleComunidade.getListaUsuariosComunidade(novaComunidade));
	}

}
