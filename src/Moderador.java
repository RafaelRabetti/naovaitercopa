import java.util.ArrayList;


public class Moderador extends Usuario{
	
	private ArrayList<Comunidade> listaComunidades;
	private ArrayList<Post> listaPosts;
	private Comunidade umaComunidade;
	private Post umPost;

	public Moderador(String umNome, String umEmail) {
		super(umNome, umEmail);
	}

	public void excluirPost(){
		listaPosts.remove(umPost);
	}
	
	public void exluirComunidade(){
		listaComunidades.remove(umaComunidade);
	}
	
}
