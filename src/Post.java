import java.util.ArrayList;


public class Post {
	
	private String nomePost;

	private ArrayList<Post> listaPosts;
	
	public Post(String umNome){
		this.nomePost = umNome;
	}

	public ArrayList<Post> getListaPosts() {
		return listaPosts;
	}

	public void setListaPosts(ArrayList<Post> listaPosts) {
		this.listaPosts = listaPosts;
	}

	public String getNomePost() {
		return nomePost;
	}

	public void setNomePost(String nomePost) {
		this.nomePost = nomePost;
	}
}
